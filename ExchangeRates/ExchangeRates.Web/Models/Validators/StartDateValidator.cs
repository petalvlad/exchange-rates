﻿using System.ComponentModel.DataAnnotations;
using System.Data;

namespace ExchangeRates.Web.Models.Validators
{
    public class StartDateValidator : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var exchangeRatesPeriod = validationContext.ObjectInstance as ExchangeRatesPeriod;
            if (exchangeRatesPeriod == null)
            {
                throw new EntityException("ExchangeRatesPeriod instance not found");
            }
            if (exchangeRatesPeriod.StartDate > exchangeRatesPeriod.EndDate)
            {
                return new ValidationResult("Start date should not exceed end date");
            }
            if (exchangeRatesPeriod.StartDate.AddMonths(2) < exchangeRatesPeriod.EndDate)
            {
                return new ValidationResult("Date period should not exceed two months, increase date");
            }
            return ValidationResult.Success;
        }
    }
}