﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data;

namespace ExchangeRates.Web.Models.Validators
{
    public class EndDateValidator : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var exchangeRatesPeriod = validationContext.ObjectInstance as ExchangeRatesPeriod;
            if (exchangeRatesPeriod == null)
            {
                throw new EntityException("ExchangeRatesPeriod instance not found");
            }
            if (exchangeRatesPeriod.EndDate > DateTime.UtcNow)
            {
                return new ValidationResult("End date should not exceed current date");
            }
            return ValidationResult.Success;
        }
    }
}