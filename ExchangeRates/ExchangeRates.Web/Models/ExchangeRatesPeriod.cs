﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using ExchangeRates.Infrastructure.Models;
using ExchangeRates.Infrastructure.Services;
using ExchangeRates.Web.Models.Validators;

namespace ExchangeRates.Web.Models
{
    public class ExchangeRatesPeriod
    {
        private readonly ExchangeRatesService _exchangeRatesService;
        private Currency _baseCurrency;
        private Currency[] _currencies;

        public Currency BaseCurrency
        {
            get { return _baseCurrency ?? (_baseCurrency = _exchangeRatesService.GetCurrencyByShortName("USD")); }
        }

        public Currency[] Currencies
        {
            get
            {
                return _currencies ?? (_currencies = _exchangeRatesService.GetCurrenciesExceptBase(BaseCurrency.ShortName).ToArray());
            }
        }

        [Required]
        [StartDateValidator]
        public DateTime StartDate { get; set; }

        [Required]
        [EndDateValidator]
        public DateTime EndDate { get; set; }

        [Required]
        public int CurrencyId { get; set; }

        public ExchangeRatesPeriod()
        {
            _exchangeRatesService = new ExchangeRatesService();
            EndDate = DateTime.UtcNow;
            StartDate = EndDate.AddMonths(-2);
        }
    }
}