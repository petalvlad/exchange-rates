﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ExchangeRates.Infrastructure.Models;
using ExchangeRates.Infrastructure.Services;
using ExchangeRates.Web.Models;

namespace ExchangeRates.Web.Controllers
{
    public class IndexController : Controller
    {
        private readonly ExchangeRatesService _exchangeRatesService;

        public IndexController()
        {
            _exchangeRatesService = new ExchangeRatesService();
        }

        public ActionResult Index()
        {
            return View(new ExchangeRatesPeriod());
        }

        [HttpPost]
        public JsonResult UpdateRates(ExchangeRatesPeriod model)
        {
            if (ModelState.IsValid)
            {
                CurrencyRate[] rates;
                
                try
                {
                    rates = _exchangeRatesService.GetCurrencyRates(model.BaseCurrency.Id, model.CurrencyId,
                                                                   model.StartDate, model.EndDate).ToArray();
                }
                catch (Exception)
                {
                    return Json(new { success = false, error = "A server error has occursed."});
                }
                
                return Json(new
                    {
                            success = true,
                            rates = rates.Select(it => new
                                {
                                        date = it.Date.ToString("yyyy-MM-dd"),
                                        rate = it.Rate
                                })
                    });
            }
            return Json(new { success = false, error = "Fix validation errors", validationErrors = GetErrorsFromModelState() });
        }

        private IEnumerable<dynamic> GetErrorsFromModelState()
        {
            // get error messages of data annatation validation attributes
            return ModelState
                    .Select(it => new
                    {
                        Field = it.Key,
                        ErrorMessages = it.Value.Errors.Select(that => that.ErrorMessage).ToArray()
                    })
                    .Where(it => it.ErrorMessages.Length > 0)
                    .ToArray();
        }
    }
}
