﻿using System.Web.Optimization;

namespace ExchangeRates.Web
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Styles").IncludeDirectory("~/Content", "*.css"));
            bundles.Add(new ScriptBundle("~/Scripts").IncludeDirectory(
                        "~/Scripts", "*.js"));
            
        }
    }
}