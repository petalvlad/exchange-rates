﻿using System.Data.Entity;
using ExchangeRates.Infrastructure.Models;

namespace ExchangeRates.Infrastructure.Migrations
{
    public class EntitiesContextInitializer : DropCreateDatabaseIfModelChanges<ExchangeRatesContext>
    {
        protected override void Seed(ExchangeRatesContext context)
        {
            using (var db = new ExchangeRatesContext())
            {
                var currencies = new[]
                    {
                            new Currency { ShortName = "RUB" },
                            new Currency { ShortName = "EUR" },
                            new Currency { ShortName = "USD" },
                            new Currency { ShortName = "GBP" },
                            new Currency { ShortName = "JPY" }
                    };

                foreach (var currency in currencies)
                {
                    db.Currencies.Add(currency);
                }

                db.SaveChanges();
            }

        }
    }
}