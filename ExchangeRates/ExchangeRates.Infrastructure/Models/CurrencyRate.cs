﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ExchangeRates.Infrastructure.Models
{
    public class CurrencyRate
    {
        [Key]
        public int Id { get; set; }

        public decimal Rate { get; set; }
        
        public int BaseCurrencyId { get; set; }
        
        public virtual Currency BaseCurrency { get; set; }
        
        public int CurrencyId { get; set; }
       
        public virtual Currency Currency { get; set; }

        public DateTime Date { get; set; }
    }
}