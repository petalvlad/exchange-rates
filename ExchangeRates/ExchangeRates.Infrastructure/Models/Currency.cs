﻿using System.ComponentModel.DataAnnotations;

namespace ExchangeRates.Infrastructure.Models
{
    public class Currency
    {
        [Key]
        public int Id { get; set; }

        public string ShortName { get; set; }
    }
}
