﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using ExchangeRates.Infrastructure.Migrations;

namespace ExchangeRates.Infrastructure.Models
{
    public class ExchangeRatesContext : DbContext
    {
        public DbSet<Currency> Currencies { get; set; }

        public DbSet<CurrencyRate> CurrencyRates { get; set; }

        static ExchangeRatesContext()
        {
            // will execute Seed method of EntitiesContextInitializer class 
            // every time the model schema is changed
            Database.SetInitializer(new EntitiesContextInitializer());
        }

        public ExchangeRatesContext()
            : base("Name=ExchangeRates")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<CurrencyRate>()
                        .Property(currencyRate => currencyRate.Date)
                        .HasColumnType("datetime2");

            modelBuilder.Entity<CurrencyRate>()
                        .Property(currencyRate => currencyRate.Rate)
                        .HasPrecision(18, 6);

            modelBuilder.Entity<CurrencyRate>()
                        .HasRequired(currencyRate => currencyRate.BaseCurrency)
                        .WithMany()
                        .HasForeignKey(currencyRate => currencyRate.BaseCurrencyId)
                        .WillCascadeOnDelete(false);


            modelBuilder.Entity<CurrencyRate>()
                        .HasRequired(currencyRate => currencyRate.Currency)
                        .WithMany()
                        .HasForeignKey(currencyRate => currencyRate.CurrencyId)
                        .WillCascadeOnDelete(false);
        }
    }
}
