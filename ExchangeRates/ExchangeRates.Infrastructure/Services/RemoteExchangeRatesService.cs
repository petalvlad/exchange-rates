﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web.Script.Serialization;
using ExchangeRates.Infrastructure.Models;
using ExchangeRates.Infrastructure.Services.Proxies;

namespace ExchangeRates.Infrastructure.Services
{
    public class RemoteExchangeRatesService
    {
        private readonly ExchangeRatesService _exchangeRatesService;

        public RemoteExchangeRatesService(ExchangeRatesService exchangeRatesService)
        {
            _exchangeRatesService = exchangeRatesService;
        }

        public IEnumerable<CurrencyRate> GetCurrencyRatesForDate(DateTime day)
        {
            var appId = ConfigurationManager.AppSettings["HighchartsAppId"];

            // load json from service
            var json = new WebClient().DownloadString(string.Format("http://openexchangerates.org/api/historical/{0}.json?app_id={1}", day.ToString("yyyy-MM-dd"), appId));
            
            var javaScriptSerializer = new JavaScriptSerializer();
            // deserialize json to proxy
            var ratesProxy = javaScriptSerializer.Deserialize<ExchangeRatesProxy>(json);

            var baseCurrency = _exchangeRatesService.GetCurrencyByShortName("USD");
            var currencies = _exchangeRatesService.GetCurrenciesExceptBase(baseCurrency.ShortName).ToArray();

            // needed for calculation the real date from UNIX timestamp
            var earlyDate = new DateTime(1970,1,1,0,0,0,0);

            // map proxy rates for each of currency supported in system
            return currencies.Select(currency => new CurrencyRate
                {
                        BaseCurrency = baseCurrency, 
                        Currency = currency, 
                        Rate = ratesProxy.rates[currency.ShortName],
                        Date = earlyDate.AddSeconds(ratesProxy.timestamp).ToUniversalTime().Date
                });
        }
    }
}