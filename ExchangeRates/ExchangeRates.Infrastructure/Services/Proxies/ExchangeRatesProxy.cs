﻿using System.Collections.Generic;

namespace ExchangeRates.Infrastructure.Services.Proxies
{
    public class ExchangeRatesProxy
    {
        public string disclaimer { get; set; }
        public string license { get; set; }
        public long timestamp { get; set; }
        public string @base { get; set; }
        public Dictionary<string, decimal> rates { get; set; }
    }
}