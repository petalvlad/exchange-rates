﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ExchangeRates.Infrastructure.Models;

namespace ExchangeRates.Infrastructure.Services
{
    public class ExchangeRatesService
    {
        private readonly ExchangeRatesContext _context;

        private readonly RemoteExchangeRatesService _remoteExchangeRatesService;

        public ExchangeRatesService()
        {
            _context = new ExchangeRatesContext();
            _remoteExchangeRatesService = new RemoteExchangeRatesService(this);
        }

        public IEnumerable<CurrencyRate> GetCurrencyRates(int baseCurrencyId, int currencyId, DateTime startDate, DateTime endDate)
        {
            var daysCount = (endDate - startDate).Days + 1;
            var rates = new List<CurrencyRate>();

            var cachedRatesDictionary = _context.CurrencyRates.Where(it =>
                                                           it.BaseCurrencyId == baseCurrencyId &&
                                                           it.CurrencyId == currencyId &&
                                                           it.Date >= startDate &&
                                                           it.Date <= endDate)
                                      .ToDictionary(it => it.Date, it => it);

            var day = startDate;
            for (var i = 0; i < daysCount; i++, day = day.AddDays(1))
            {
                var aDay = day;
                // if desired currency rate exists in database
                if (cachedRatesDictionary.ContainsKey(aDay))
                {
                    rates.Add(cachedRatesDictionary[aDay]);
                }
                else
                {
                    // load currency rates from remote service
                    var currencyRates = _remoteExchangeRatesService.GetCurrencyRatesForDate(aDay).Where(it => it.Date == aDay.Date).ToArray();
                    // save them to database
                    if (currencyRates.Length > 0)
                    {
                        SaveRatesToDataBase(currencyRates);
                        // and grab desired rate
                        rates.Add(currencyRates.Single(it => it.Currency.Id == currencyId));
                    }
                }
            }
            return rates.OrderBy(it => it.Date);
        }

        private void SaveRatesToDataBase(IEnumerable<CurrencyRate> currencyRates)
        {
            foreach (var currencyRate in currencyRates)
            {
                _context.CurrencyRates.Add(currencyRate);
            }
            _context.SaveChanges();
        }

        public IEnumerable<Currency> GetCurrenciesExceptBase(string shortName)
        {
            return _context.Currencies.Where(it => it.ShortName != shortName);
        }

        public Currency GetCurrencyByShortName(string shortName)
        {
            return _context.Currencies.First(it => it.ShortName == shortName);
        }
    }
}